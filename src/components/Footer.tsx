import Link from 'next/link';
import FooterContent from './FooterContent';

type Props = {};

const Footer = (props: Props) => {
  return (
    <footer className="bg-white text-black p-10">
      <div className="flex justify-between">
        <FooterContent />
      </div>
    </footer>
  );
};

export default Footer;