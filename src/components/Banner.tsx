import Image from "next/image";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";

type Props = {};

const Banner: React.FC<Props> = (props: Props) => {
  return (
    <div className="relative">
      <div className="absolute w-full h-1/3 bg-gradient-to-b from-transparent to-gray-100 bottom-0 z-20" />
      <Carousel
        autoPlay
        infiniteLoop
        showStatus={false}
        showIndicators={false}
        showThumbs={false}
        interval={10000}
        transitionTime={1000}
        stopOnHover={false}
      >
        <div className="relative h-96 md:h-[600px] lg:h-[700px]">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel1.jpg"
            alt="carousel1"
            priority
          />
        </div>
        <div className="relative h-96 md:h-[600px] lg:h-[700px]">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel2.jpg"
            alt="carousel2"
            priority
          />
        </div>
        <div className="relative h-96 md:h-[600px] lg:h-[700px]">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel3.jpg"
            alt="carousel3"
            priority
          />
        </div>
        <div className="relative h-96 md:h-[600px] lg:h-[700px]">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel4.jpg"
            alt="carousel4"
            priority
          />
        </div>
        <div className="relative h-96 md:h-[600px] lg:h-[700px]">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel5.jpg"
            alt="carousel5"
            priority
          />
        </div>
      </Carousel>
    </div>
  );
};

export default Banner;