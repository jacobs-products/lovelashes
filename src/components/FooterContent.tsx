// You are free to edit this file in order to modify the footer
import React from 'react';
import Link from 'next/link';

const FooterContent: React.FC = () => {
  return (
    <div className="flex flex-col space-y-2">
      <h4 className="font-bold mb-2 text-black">Get to Know Us</h4>
      <ul className="text-black">
        <li>
          <Link href="/about">
            <a className="link">About Us</a>
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default FooterContent;