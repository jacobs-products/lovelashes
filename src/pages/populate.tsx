import { useEffect, useState } from "react";
import { GetServerSidePropsContext } from "next";
import { getSession, useSession } from "next-auth/react";
import Head from "next/head";
import { IProduct, ISession } from "../../typings";
import Header from "../components/Header";
import { collection, getDocs, addDoc, updateDoc, deleteDoc, query, where, serverTimestamp, doc } from 'firebase/firestore';
import db from '../../firebase';
import { useProductContext } from "components/context/ProductContext";

const Populate = () => {
  const { products, loading, error } = useProductContext();
  const { data: session } = useSession();
  const [isCreatingCategory, setIsCreatingCategory] = useState(false);
  const [isCreatingSubcategory, setIsCreatingSubcategory] = useState(false);
  const [isCreatingSubsubcategory, setIsCreatingSubsubcategory] = useState(false);
  const [categoryChanged, setCategoryChanged] = useState(false);
  const [subcategoryChanged, setSubcategoryChanged] = useState(false);

  
  if (!process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL) {
    console.error('NEXT_PUBLIC_AUTHORIZED_EMAIL environment variable is not set');
    return null;
  }
  
  const authorizedEmails = process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL.includes(',')
    ? process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL.split(',')
    : [process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL];

  // Check if the logged-in user is authorized
  const isAuthorized = authorizedEmails.includes(session?.user.email as string);

  const [newProduct, setNewProduct] = useState<IProduct>({
    id: '',
    title: '',
    affiliateLink: '',
    price: 0,
    description: '',
    category: '',
    image: '',
    quantity: 0,
    subcategory: '',
    subsubcategory: '',
    isCreatingCategory: false,
    isCreatingSubcategory: false,
    isCreatingSubsubcategory: false,
  });

  // State to keep track of the edited product fields
  const [editedProducts, setEditedProducts] = useState<IProduct[]>(products);

  const [removedProducts, setRemovedProducts] = useState<IProduct[]>([]);

  const [selectedFile, setSelectedFile] = useState<File | null>(null);

  const [selectedFiles, setSelectedFiles] = useState<(File | null)[]>(new Array(editedProducts.length).fill(null));

  const [newProductButtonText, setNewProductButtonText] = useState("");

  const [editedProductsButtonText, setEditedProductsButtonText] = useState("");
  
  const handleFileChange = (file: File) => {
    setSelectedFile(file);
  };

  const handleFilesChange = (index: number, file: File) => {
    setSelectedFiles(prevState => {
      const newSelectedFiles = [...prevState];
      newSelectedFiles[index] = file;
      return newSelectedFiles;
    });
  };  

  // Function to handle updating a field in the new product form
  const handleUpdateNewProductField = (field: keyof IProduct, value: string | number) => {
    setNewProduct(prevState => ({
      ...prevState,
      [field]: value,
    }));
  }

  // Function to handle updating a product field
  const handleUpdateProductField = (index: number, field: keyof IProduct, value: string | number | boolean) => {
    setEditedProducts(prevState => {
      const newProducts = [...prevState];
      (newProducts[index] as any)[field] = value;
      return newProducts;
    });
  }

  const handleRemoveProduct = (productId: string) => {
    setEditedProducts(prevState => {
      const newProducts = prevState.filter(product => product.id !== productId);
      const removedProduct = prevState.find(product => product.id === productId);
      if (removedProduct) {
        setRemovedProducts(prevState => [...prevState, removedProduct]);
      }
      return newProducts;
    });
  }

  
  const handleSelectDefaultSubcategory = (index: any) => {
    const subcategoryElement = document.getElementById("subcategory"+index) as any;
    console.log(subcategoryElement);
    subcategoryElement.selectedIndex = 0;
    const event = new Event('change', { bubbles: true });
    subcategoryElement.dispatchEvent(event);
  };

  const handleSelectDefaultSubsubcategory = (index: any) => {
    const subsubcategoryElement = document.getElementById("subsubcategory"+index) as any;
    console.log(subsubcategoryElement);
    subsubcategoryElement.selectedIndex = 0;
    const event = new Event('change', { bubbles: true });
    subsubcategoryElement.dispatchEvent(event);
  };

  // Function to handle submitting the new product form
  const handleSubmitNewProduct = async () => {

    setNewProductButtonText("Please wait 5s.");
    let counter = 5;
    const intervalId = setInterval(() => {
      counter--;
      setNewProductButtonText(`Please wait ${counter}s.`);
      if (counter === 0) {
        clearInterval(intervalId);
        setNewProductButtonText("Finished, please refresh the page a couple of times until the changes appear.");
      }
    }, 1000);

    // Upload the selected file to ImgBB using their API and get the resulting image URL
    let imageUrl = '';
    if (selectedFile) {
      const formData = new FormData();
      formData.append('image', selectedFile);
      const response = await fetch(`https://api.imgbb.com/1/upload?key=${process.env.NEXT_PUBLIC_IMGBB_API_KEY}`, {
        method: 'POST',
        body: formData,
      });
      const data = await response.json();
      imageUrl = data.data.url;
    }
  
    // Query the products collection to get all existing products
    const querySnapshot = await getDocs(collection(db, 'products'));
    // Find the highest id value among the existing products
    let maxId = 0;
    querySnapshot.forEach(doc => {
      const product = doc.data() as IProduct;
      const id = parseInt(product.id);
      if (id > maxId) {
        maxId = id;
      }
    });
    // Generate a new unique id by incrementing the highest id value by 1
    const newId = (maxId + 1).toString();
    // Add a new document to the products collection with the generated id
    await addDoc(collection(db, 'products'), {
      ...newProduct,
      id: newId,
      image: imageUrl,
      description: newProduct.description ?? '', // Set description to an empty string if it is undefined
      quantity: newProduct.quantity ?? 0, // Set quantity to 0 if it is undefined
      subcategory: newProduct.subcategory ?? '', // Set subcategory to an empty string if it is undefined
      subsubcategory: newProduct.subsubcategory ?? '', // Set subsubcategory to an empty string if it is undefined
    });
    // Clear the input fields and selected file
    setNewProduct({
      id: '',
      title: '',
      price: 0,
      description: '',
      category: '',
      image: '',
      quantity: 0,
      subcategory: '',
      subsubcategory: '',
    });
    setSelectedFile(null);
  
    // Update the last updated timestamp for the products collection
    const lastUpdatedRef = collection(db, 'lastUpdated');
    const lastUpdatedQuery = query(lastUpdatedRef, where('type', '==', 'products'));
    const lastUpdatedSnapshot = await getDocs(lastUpdatedQuery);
    const lastUpdatedDocRef = lastUpdatedSnapshot.docs[0]?.ref;
    await updateDoc(lastUpdatedDocRef, { timestamp: serverTimestamp() });
  };  

  // Function to handle submitting the changes
  const handleSubmitChanges = async () => {
    
    setEditedProductsButtonText("Please wait 5s.");
    let counter = 5;
    const intervalId = setInterval(() => {
      counter--;
      setEditedProductsButtonText(`Please wait ${counter}s.`);
      if (counter === 0) {
        clearInterval(intervalId);
        setEditedProductsButtonText("Finished, please refresh the page a couple of times until the changes appear.");
      }
    }, 1000);

    // Loop through the edited products
    for (const [index, product] of editedProducts.entries()) {
      // Upload the selected file to ImgBB using their API and get the resulting image URL
      let imageUrl = product.image;
      if (selectedFiles[index]) {
        const formData = new FormData();
        formData.append('image', selectedFiles[index] as File);
        const response = await fetch(`https://api.imgbb.com/1/upload?key=${process.env.NEXT_PUBLIC_IMGBB_API_KEY}`, {
          method: 'POST',
          body: formData,
        });
        const data = await response.json();
        imageUrl = data.data.url;
      }
  
      // Query the products collection for a document with a matching product.id field
      const querySnapshot = await getDocs(query(collection(db, 'products'), where('id', '==', product.id)));
      // Get the first document from the query result (there should only be one)
      const docSnapshot = querySnapshot.docs[0];
      if (docSnapshot) {
        // Get a reference to the product document
        const productRef = docSnapshot.ref;
        // Update the product document with the new information
        await updateDoc(productRef, {
          title: product.title,
          affiliateLink: product.affiliateLink ?? '',
          price: product.price,
          description: product.description ?? '', // Set description to an empty string if it is undefined
          category: product.category,
          image: imageUrl,
          quantity: product.affiliateLink ? product.quantity : product.quantity ?? 0,
          subcategory: product.subcategory ?? '', // Set subcategory to an empty string if it is undefined
          subsubcategory: product.subsubcategory ?? '', // Set subsubcategory to an empty string if it is undefined
        });
      }
    }
    // Loop through the removed products
    for (const product of removedProducts) {
      // Query the products collection for a document with a matching product.id field
      const querySnapshot = await getDocs(query(collection(db, 'products'), where('id', '==', product.id)));
      // Get the first document from the query result (there should only be one)
      const docSnapshot = querySnapshot.docs[0];
      if (docSnapshot) {
        // Get a reference to the product document
        const productRef = docSnapshot.ref;
        // Delete the product document
        await deleteDoc(productRef);
      } else {
        console.log(`No document found with id ${product.id}`);
      }
    }
  
    // Update the last updated timestamp for the products collection
    const lastUpdatedRef = collection(db, 'lastUpdated');
    const lastUpdatedQuery = query(lastUpdatedRef, where('type', '==', 'products'));
    const lastUpdatedSnapshot = await getDocs(lastUpdatedQuery);
    const lastUpdatedDocRef = lastUpdatedSnapshot.docs[0]?.ref;
    await updateDoc(lastUpdatedDocRef, { timestamp: serverTimestamp() });
  };  

  return (
    <div className="bg-gradient-to-r from-yellow-200 via-orange-200 to-red-200 min-h-screen">
      <Header />
      {isAuthorized ? (
        // Render page content for authorized users
        <main className="max-w-screen-2xl mx-auto">
          {/* Header */}
          <Head>
            <title>Populate Products</title>
          </Head>
          <h2>When adding or editing a product, please allow roughly 5 seconds until the form clears before navigating away from this page.</h2>
          {/* New product form */}
          <div className="m-5 bg-white z-30 p-10">
            <h2>New Product</h2>
            {/* Render input fields for each product field (except id) */}
            <label>Title: *</label>
            <input
              type="text"
              value={newProduct.title}
              onChange={e => handleUpdateNewProductField('title', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            <label>Affiliate Link:</label>
            <input
              type="text"
              value={newProduct.affiliateLink}
              onChange={e => handleUpdateNewProductField('affiliateLink', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            <label>Price: *</label>
            <input
              type="number"
              value={newProduct.price}
              onChange={e => handleUpdateNewProductField('price', parseFloat(e.target.value))}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            <label>Description:</label>
            <input
              type="text"
              value={newProduct.description}
              onChange={e => handleUpdateNewProductField('description', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            <label>Category: *</label>
            {isCreatingCategory ? (
              <>
              <input
              type="text"
              value={newProduct.category}
              onChange={e => handleUpdateNewProductField('category', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
              />
              <br />
              <button
              onClick={() => {
                setIsCreatingCategory(false);
                handleUpdateNewProductField('category', "");

              }}
              className="mt-2 button"
              >
              Select Existing Category
              </button>
              </>
            ) : (
              <>
              <select
              value={newProduct.category}
              onChange={e => handleUpdateNewProductField('category', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
              >
              <option value="" selected>Select Category</option>
              {Array.from(new Set(products.map(product => product.category))).map(category => (
              <option key={category} value={category}>
              {category}
              </option>
              ))}
              </select>
              <br />
              <button
              onClick={() => {
                setIsCreatingCategory(true);
                handleUpdateNewProductField('category', "");
              }}
              className="mt-2 button"
              >
              Create New Category
              </button>
              </>
            )}
            <br />
            <label>Subcategory: </label>
            {isCreatingSubcategory ? (
                <>
                <input
                type="text"
                value={categoryChanged ? "" : newProduct.subcategory}
                onChange={e => {
                handleUpdateNewProductField('subcategory', e.target.value);
                setCategoryChanged(false);
                }}
                className="border border-gray-300 rounded-md p-1"
                />
                <br />
                <button
                onClick={() => {
                  setIsCreatingSubcategory(false);
                  handleUpdateNewProductField('subcategory', "");

                }}
                className="mt-2 button"
                >
                Select Existing Subcategory
                </button>
                </>
            ) : (
              <>
              <select
              value={newProduct.subcategory}
              onChange={e => handleUpdateNewProductField('subcategory', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
              >
              <option value="" selected>Select Subcategory</option>
              {Array.from(new Set(products
              .filter(product => product.category === newProduct.category && product.subcategory != "")
              .map(product => product.subcategory)
              )).map(subcategory => (
              <option key={subcategory} value={subcategory}>
              {subcategory}
              </option>
              ))}
              </select>
              <br />
              <button
              onClick={() => {
                setIsCreatingSubcategory(true);
                handleUpdateNewProductField('subcategory', "");
              }}
              className="mt-2 button"
              >
              Create New Subcategory
              </button>
              </>
            )}
            <br />
            <label>Sub-subcategory: </label>
            {isCreatingSubsubcategory ? (
                <>
                <input
                type="text"
                value={subcategoryChanged ? "" : newProduct.subsubcategory}
                onChange={e => {
                handleUpdateNewProductField('subsubcategory', e.target.value);
                setSubcategoryChanged(false);
                }}
                className="border border-gray-300 rounded-md p-1"
                />
                <br />
                <button
                onClick={() => {
                  setIsCreatingSubsubcategory(false);
                  handleUpdateNewProductField('subsubcategory', "");
                }}
                className="mt-2 button"
                >
                Select Existing Sub-subcategory
                </button>
                </>
            ) : (
              <>
              <select
              value={newProduct.subsubcategory}
              onChange={e => handleUpdateNewProductField('subsubcategory', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
              >
                <option value="" selected>Select Sub-subcategory</option>
              {Array.from(new Set(products
              .filter(product => product.category === newProduct.category && product.subcategory === newProduct.subcategory && product.subsubcategory != "")
              .map(product => product.subsubcategory)
              )).map(subsubcategory => (
              <option key={subsubcategory} value={subsubcategory}>
              {subsubcategory}
              </option>
              ))}
              </select>
              <br />
              <button
              onClick={() => {
                setIsCreatingSubsubcategory(true);
                handleUpdateNewProductField('subsubcategory', "");
              }}
              className="mt-2 button"
              >
              Create New Sub-subcategory
              </button>
              </>
            )}
            <br />
            <label>Image: *</label>
            <input
              type="file"
              onChange={e => {
                if (e.target.files) {
                  handleFileChange(e.target.files[0]);
                }
              }}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            <label>Quantity (leave blank if affiliate link is added): *</label>
            <input
              type="number"
              value={newProduct.quantity}
              onChange={e => handleUpdateNewProductField('quantity', parseInt(e.target.value))}
              className="border border-gray-300 rounded-md p-1"
            />
            {/* Submit button for New Product */}
            <button
              onClick={() => {
                if (
                  newProduct.title &&
                  newProduct.price &&
                  newProduct.category &&
                  (newProduct.image || selectedFile) &&
                  (newProduct.quantity || newProduct.quantity === 0)
                ) {
                  handleSubmitNewProduct();
                } else {
                  alert('Please fill in all required fields');
                }
              }}
              className="mt-auto button mx-auto mb-10"
            >
              Submit New Product
            </button>
            <p>{newProductButtonText}</p>
          </div>
          {/* Product editor */}
          <div className="grid grid-flow-row-dense md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            {editedProducts.map((editedProduct, index) => (
              <div key={editedProduct.id} className="relative flex flex-col m-5 bg-white z-30 p-10">
                {/* X button to remove product */}
                <button onClick={() => handleRemoveProduct(editedProduct.id)} className="absolute top-2 right-2 text-lg font-bold">
                  X
                </button>
                {/* Render input fields for each product field (except id) */}
                <label>Title: *</label>
                <input
                  type="text"
                  value={editedProduct.title}
                  onChange={e => handleUpdateProductField(index, 'title', e.target.value)}
                  className="border border-gray-300 rounded-md p-1"
                />
                <label>Affiliate Link:</label>
                <input
                  type="text"
                  value={editedProduct.affiliateLink}
                  onChange={e => handleUpdateProductField(index, 'affiliateLink', e.target.value)}
                  className="border border-gray-300 rounded-md p-1"
                />
                <br />
                <label>Price: *</label>
                <input
                  type="number"
                  value={editedProduct.price}
                  onChange={e => handleUpdateProductField(index, 'price', parseFloat(e.target.value))}
                  className="border border-gray-300 rounded-md p-1"
                />
                <br />
                <label>Description:</label>
                <input
                  type="text"
                  value={editedProduct.description}
                  onChange={e => handleUpdateProductField(index, 'description', e.target.value)}
                  className="border border-gray-300 rounded-md p-1"
                />
                <br />
                <label>Category: *</label>
                {editedProduct.isCreatingCategory ? (
                  <>
                  <input
                  type="text"
                  value={editedProduct.category}
                  onChange={e => handleUpdateProductField(index, 'category', e.target.value)}
                  className="border border-gray-300 rounded-md p-1"
                  />
                  <br />
                  <button
                  onClick={() => {
                    handleUpdateProductField(index, 'isCreatingCategory', false);
                    handleUpdateProductField(index, 'category', "");
                  }}
                  className="mt-2 button"
                  >
                  Select Existing Category
                  </button>
                  </>
                ) : (
                  <>
                  <select
                  value={editedProduct.category}
                  onChange={e => {
                    handleUpdateProductField(index, 'category', e.target.value);
                    handleSelectDefaultSubcategory(index);
                    handleSelectDefaultSubsubcategory(index);
                  }}
                  className="border border-gray-300 rounded-md p-1"
                  >
                  <option value="" selected>Select Category</option>
                  {Array.from(new Set(products.map(product => product.category))).map(category => (
                  <option key={category} value={category}>
                  {category}
                  </option>
                  ))}
                  </select>
                  <br />
                  <button
                  onClick={() => {
                    handleUpdateProductField(index, 'isCreatingCategory', true);
                    handleUpdateProductField(index, 'category', "");
                    handleSelectDefaultSubcategory(index);
                    handleSelectDefaultSubsubcategory(index);
                  }}
                  className="mt-2 button"
                  >
                  Create New Category
                  </button>
                  </>
                )}
                <br />
                <label>Subcategory: </label>
                {editedProduct.isCreatingSubcategory ? (
                  <>
                  <input
                  type="text"
                  value={editedProduct.subcategory}
                  onChange={e => handleUpdateProductField(index, 'subcategory', e.target.value)}
                  className="border border-gray-300 rounded-md p-1"
                  />
                  <br />
                  <button
                  onClick={() => {
                    handleUpdateProductField(index, 'isCreatingSubcategory', false);
                    handleUpdateProductField(index, 'subcategory', "");
                  }}
                  className="mt-2 button"
                  >
                  Select Existing Subcategory
                  </button>
                  </>
                ) : (
                  <>
                  <select
                  value={editedProduct.subcategory}
                  id={"subcategory"+index}
                  onChange={e => {
                    handleUpdateProductField(index, 'subcategory', e.target.value);
                    handleSelectDefaultSubsubcategory(index);
                  }}
                  className="border border-gray-300 rounded-md p-1"
                  >
                  <option value="" selected>Select Subcategory</option>
                  {Array.from(new Set(products
                  .filter(product => product.category === editedProduct.category && product.subcategory != "")
                  .map(product => product.subcategory)
                  )).map(subcategory => (
                  <option key={subcategory} value={subcategory}>
                  {subcategory}
                  </option>
                  ))}
                  </select>
                  <br />
                  <button
                  onClick={() => {
                    handleUpdateProductField(index, 'isCreatingSubcategory', true);
                    handleUpdateProductField(index, 'subcategory', "");
                    handleSelectDefaultSubsubcategory(index);
                  }}
                  className="mt-2 button"
                  >
                  Create New Subcategory
                  </button>
                  </>
                )}
                <br />
                <label>Sub-subcategory: </label>
                {editedProduct.isCreatingSubsubcategory ? (
                  <>
                  <input
                  type="text"
                  value={editedProduct.subsubcategory}
                  onChange={e => handleUpdateProductField(index, 'subsubcategory', e.target.value)}
                  className="border border-gray-300 rounded-md p-1"
                  />
                  <br />
                  <button
                  onClick={() => {
                    handleUpdateProductField(index, 'isCreatingSubsubcategory', false);
                    handleUpdateProductField(index, 'subsubcategory', "");
                  }}
                  className="mt-2 button"
                  >
                  Select Existing Sub-subcategory
                  </button>
                  </>
                ) : (
                  <>
                  <select
                  value={editedProduct.subsubcategory}
                  id={"subsubcategory"+index}
                  onChange={e => handleUpdateProductField(index, 'subsubcategory', e.target.value)}
                  className="border border-gray-300 rounded-md p-1"
                  >
                  <option value="" selected>Select Sub-subcategory</option>
                  {Array.from(new Set(products
                  .filter(product => product.category === editedProduct.category && product.subcategory === editedProduct.subcategory && product.subsubcategory != "")
                  .map(product => product.subsubcategory)
                  )).map(subsubcategory => (
                  <option key={subsubcategory} value={subsubcategory}>
                  {subsubcategory}
                  </option>
                  ))}
                  </select>
                  <br />
                  <button
                  onClick={() => {
                    handleUpdateProductField(index, 'isCreatingSubsubcategory', true);
                    handleUpdateProductField(index, 'subsubcategory', "");
                  }}
                  className="mt-2 button"
                  >
                  Create New Sub-subcategory
                  </button>
                  </>
                )}
                <br />
                <label>Image: *</label>
                  <img src={editedProduct.image} alt={editedProduct.title} className="object-contain w-252 h-350 mx-auto" />
                  <input
                    type="file"
                    onChange={e => {
                      if (e.target.files) {
                        handleFilesChange(index, e.target.files[0]);
                      }
                    }}
                    className="border border-gray-300 rounded-md p-1"
                  />
                <br />
                <label>Quantity: *</label>
                <input
                  type="number"
                  value={editedProduct.quantity}
                  onChange={e => handleUpdateProductField(index, 'quantity', parseInt(e.target.value))}
                  className="border border-gray-300 rounded-md p-1"
                />
              </div>
            ))}
          </div>
          {/* Submit button for Changes */}
          <button
            onClick={() => {
              let allFieldsFilled = true;
              editedProducts.forEach(product => {
                if (!product.title || !product.price || !product.category || newProduct.image) {
                  allFieldsFilled = false;
                }
              });
              if (allFieldsFilled) {
                handleSubmitChanges();
              } else {
                alert('Please fill in all required fields');
              }
            }}
            className="mt-auto button mx-auto mb-10"
          >
            Submit Changes
          </button>
          <p>{editedProductsButtonText}</p>
      </main>
      ) : (
        // Render placeholder image for unauthorized users
        <img
          src="https://i.redd.it/70jk8jtyl6a31.jpg"
          alt="Move along, nothing to see here."
          style={{
            display: 'block',
            maxWidth: '80%',
            maxHeight: '80%',
            width: 'auto',
            height: 'auto',
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)'
          }}
        />
      )}
    </div>
  );
};

export default Populate;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  // Get user logged in credentials
  const session: ISession | null = await getSession(context);
  return {
    props: {
      session,
    },
  };
};