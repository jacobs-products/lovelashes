import React, { useEffect } from "react";
import { CheckCircleIcon } from "@heroicons/react/24/solid";
import { useRouter } from "next/router";
import Header from "../components/Header";
import { useDispatch } from "react-redux";
import { emptyBasket } from "../slices/basketSlice";

type Props = {};

const Success: React.FC<Props> = (props: Props) => {
  const router = useRouter();
  const dispatch = useDispatch();

  useEffect(() => {
    // Empty the user's basket
    dispatch(emptyBasket());
  }, [dispatch]);

  return (
    <div className="bg-gradient-to-r from-yellow-200 via-orange-200 to-red-200 min-h-screen">
      <Header />

      <main className="max-screen-lg mx-auto">
        <div className="flex flex-col p-10 bg-white">
          <div className="flex items-center space-x-2 mb-5">
            <CheckCircleIcon className="text-green-500 h-10" />
            <h1 className="text-3xl">
              Thank you, your order has been confirmed!
            </h1>
          </div>
          <p>
            Thank you for shopping with us. If you would like to see your order(s), please press the link below.
          </p>
          <button onClick={() => router.push("/orders")} className="button mt-8 bg-[#b200c4] hover:bg-[#9e00b0]">
            Go to my orders
          </button>
        </div>
      </main>
    </div>
  );
};

export default Success;