import React from 'react';
import Head from 'next/head';
import Header from '../components/Header';

type Props = {};

const About = (props: Props) => {
  return (
    <>
      <Head>
        <title>About | Lux-Juliet</title>
      </Head>
      <Header />
      <main className="bg-gradient-to-r from-yellow-200 via-orange-200 to-red-200 min-h-screen">
        <div className="max-w-screen-2xl mx-auto p-10">
          <h1 className="text-3xl font-bold mb-6 text-black">About Lux-Juliet</h1>
          <p className="text-lg mb-4 text-black">
            Lux-Juliet is a business that sells all sorts of fashion items.
          </p>
          {/* ... */}
        </div>
      </main>
    </>
  );
};

export default About;