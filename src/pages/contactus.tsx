import React from 'react';
import Head from 'next/head';
import Header from '../components/Header';

const Contact = () => {
  return (
    <div className="bg-gradient-to-r from-yellow-200 via-orange-200 to-red-200 min-h-screen">
      <Head>
        <title>Lux-Juliet</title>
      </Head>
      <Header />
      <br />
      <div className="flex flex-col items-center">
        <iframe
          src="https://docs.google.com/forms/d/e/1FAIpQLSe8UEwHaYemfBZNF9iv_4nAElJqmZHo_xdgG8oeun2B5JSY3w/viewform?embedded=true"
          className="responsive-iframe"
        >
          Loading…
        </iframe>
      </div>
      <style jsx>{`
        .responsive-iframe {
          width: 80%;
          max-width: 640px;
          height: 100vh;
          max-height: 875px;
        }

        @media screen and (max-width: 768px) {
          .responsive-iframe {
            width: 80%;
            height: 80vh;
          }
        }
      `}</style>
    </div>
  );
};

export default Contact;