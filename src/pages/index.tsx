import { GetServerSidePropsContext } from 'next';
import Head from 'next/head';
import { useEffect, useState } from 'react';
import Header from '../components/Header';
import BlogFeed from '../components/BlogFeed';
import { getSession } from 'next-auth/react';
import { Blog } from "../../typings";
import matter from 'gray-matter';
import Banner from 'components/Banner';

type Props = {
  session: any; // Define the correct type for your session object
  initialBlogs: Blog[]; // Add a new prop for the initial blog data
};

const Home = ({ session, initialBlogs }: Props) => {
  
  if (!process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL) {
    console.error('NEXT_PUBLIC_AUTHORIZED_EMAIL environment variable is not set');
    return null;
  }
  
  const authorizedEmails = process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL.includes(',')
    ? process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL.split(',')
    : [process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL];
  
  // Check if the logged-in user is authorized

  const [isAuthorized, setIsAuthorized] = useState(false);
  const [blogs, setBlogs] = useState<Blog[]>(initialBlogs); // Initialize the blogs state with the initialBlogs prop

  useEffect(() => {
    if (authorizedEmails.includes(session?.user.email as string)) {
      setIsAuthorized(true);
    }
  }, [session]);

  const handleBlogSubmit = (newBlog: Blog) => {
    // This is the function that handles blog submissions
    // You can update your state or make an API request to save the new blog
    setBlogs(prevBlogs => [...prevBlogs, newBlog]);
  };

  return (
    <div className="bg-gradient-to-r from-yellow-200 via-orange-200 to-red-200 min-h-screen">
      <Head>
      </Head>
      <Header />
      <main className="max-w-screen-2xl mx-auto">
        <Banner />
        <BlogFeed blogs={blogs} /> {/* Display blogs */}
      </main>
    </div>
  );
};

export default Home;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const session = await getSession(context);

  try {
    console.log("start");
    const res = await fetch(`https://api.netlify.com/api/v1/sites/${process.env.NETLIFY_SITE_ID}/files`, {
      headers: {
        'Authorization': `Bearer ${process.env.GITLABS_ACCESS_TOKEN}`
      }
    });
    if (!res.ok) {
      console.error(`API request failed with status ${res.status}: ${res.statusText}`);
      throw new Error(`API request failed with status ${res.status}`);
    }
    
    interface File {
      id: string;
      path: string;
      sha: string;
    }
    
    const files = (await res.json()) as File[];
    
    const blogFiles = files.filter(file => file.path.startsWith('/content/blog/'));
    
    // Fetch the content of each blog file
    const initialBlogs = await Promise.all(
      blogFiles.map(async file => {
        const downloadUrl = `${process.env.REPO_URL}/raw/main/public${file.path}`;
        const res = await fetch(downloadUrl, {
          headers: {
            'Authorization': `Bearer ${process.env.GITLABS_ACCESS_TOKEN}}`
          }
        });
        if (!res.ok) {
          console.error(`Failed to fetch file content for ${file.path}`);
          console.log(downloadUrl);
          return null;
        }
        const markdownContent = await res.text();
        
        // Parse the markdown content to extract the metadata and body
        const { data } = matter(markdownContent);
        console.log("file contents is: " + file.sha + " " + data.title + " " + data.author, " " + data.content, " " + data.date)
        return {
          id: file.sha,
          title: data.title,
          author: data.author,
          content: data.content,
          date: data.date,
        };
      })
    );

    return {
      props: {
        session,
        initialBlogs,
      },
    };
  } catch (error) {
    console.error(`Error fetching blog data from API: ${error}`);
    return {
      props: {
        session,
        initialBlogs: [],
      },
    };
  }
};