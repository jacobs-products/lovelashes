export interface IProduct {
  id: string;
  title: string;
  affiliateLink?: string;
  price: number;
  description?: string;
  category: string;
  subcategory?: string;
  subsubcategory?: string;
  isCreatingCategory?: boolean;
  isCreatingSubcategory?: boolean;
  isCreatingSubsubcategory?: boolean;
  image: string;
  quantity: number;
}

export interface IOrder {
  id: number;
  title: string;
  amount: number;
  amount_shipping: number;
  timestamp: number;
  images: string[];
}

export interface ISession {
  user: {
    name: string;
    email: string;
    image: string;
    address: string;
  } & DefaultSession["user"];
  expires: string;
}

export type Blog = {
  id: number;
  title: string;
  author: string;
  content: string;
  date: string;
};